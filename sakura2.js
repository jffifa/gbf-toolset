'use strict';

var autoBattle = {
    /*
    _enabled = false,
    //获取jquery
    _jq: function(_) {
        if (typeof _ == 'function') {
            return _();
        } else if (typeof _ == 'string') {
            return $(_);
        } else {
            return _;
        }
    },
    */
    QUEST: 'quest/supporter/710081/5',
    SUP_TYPE: 4,
    SUP_STRATEGY: [
        {
            summonImg:'2040006000', //召唤石头像编号(80)
            summonEvo:'3' //召唤石突破等级
        },
        {
            summonImg:'2040042000', //召唤石头像编号(nezha)
            summonEvo:'3' //召唤石突破等级
        },
        {
            summonImg:'2040020000', //召唤石头像编号(bitch of wind)
            summonEvo:'3' //召唤石突破等级
        },
        {
            summonImg:'2040006000', //召唤石头像编号
            summonEvo:'2' //召唤石突破等级
        },
        {
            summonImg:'2040042000', //召唤石头像编号
            summonEvo:'2' //召唤石突破等级
        },
        {
            summonImg:'2040020000', //召唤石头像编号
            summonEvo:'2' //召唤石突破等级
        }
    ],
    _started: false,
    HEAL_THRES: 0.4,

    sleep: function(sec) {
        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve();
            }, sec*1000)
        }.bind(this));
    },

    waitUntilInvisible: function(selector) {
        return new Promise(function(resolve) {
            if (!$(selector).is(":visible")) {
                resolve();
                return;
            }
            this.invisibleObserver && this.invisibleObserver.disconnect();
            var self = this;
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if (!$(selector).is(":visible")) {
                        this.invisibleObserver && this.invisibleObserver.disconnect();
                        resolve();
                    }
                }, this);
            }.bind(this));

            observer.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: true,
                characterData: false
            });
            this.invisibleObserver = observer;
        }.bind(this));
    },

    waitUntilVisible: function(selector) {
        return new Promise(function(resolve) {
            if ($(selector).is(":visible")) {
                resolve();
                return;
            }
            this.visibleObserver && this.visibleObserver.disconnect();
            var self = this;
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if ($(selector).is(":visible")) {
                        this.visibleObserver && this.visibleObserver.disconnect();
                        resolve();
                    }
                }, this);
            }.bind(this));

            observer.observe(document.body, {
                childList: true
                , subtree: true
                , attributes: true
                , characterData: false
            });
            this.visibleObserver = observer;
        }.bind(this));
    },

    waitOnce: function(selector) {
        return new Promise(function(resolve) {
            if ($(selector).size()>0) {
                console.log('found existing ' + selector);
                resolve();
                return;
            }
            this.onceObserver && this.onceObserver.disconnect();
            var self = this;
            var observer = new MutationObserver(function(mutations) {
                if ($(selector).size()>0) {
                    console.log('found ' + selector);
                    this.onceObserver && this.onceObserver.disconnect();
                    resolve();
                }
            }.bind(this));

            observer.observe(document.body, {
                childList: true
                , subtree: true
                , attributes: true
                , characterData: false
            });
            this.onceObserver = observer;
        }.bind(this));
    },

    //不负责waitOnce
    click: function(selector) {
        console.log('clicking ' + selector);
        this.sleep(1 + Math.random()).then(function() {
            if ($(selector).size()>0) {
                $(selector).trigger('tap');
            }
        }.bind(this));
    },

    getPage: function(){return window.location.hash.replace('#', '');},
    gotoPage: function(page) {window.location.hash='#'+page;},

    //吃AP药: 0 full, 1 half
    restoreAP: function(apType, callback){
        this.gotoPage('item');
        this.waitOnce('.lis-item').then(function(){
            return this.sleep(1);
        }.bind(this)).then(function(){
            var apSel = '#prt-normal .lis-item.se[data-index='+
                apType.toString()+']';
            this.click(apSel);
            return this.waitOnce('.btn-usual-use');
        }.bind(this)).then(function(){
            this.click('.btn-usual-use');
            return this.sleep(1);
        }.bind(this)).then(function(){
            console.log('AP item type '+apType.toString()+' used');
            if (callback) {
                return this.waitOnce('.btn-usual-close').then(function(){
                    return callback();
                }.bind(this));
            } else {
                return Promise.resolve();
            }
        }.bind(this));
    },

    //选召唤，确认AP
    chooseSupporter: function(supType, supStrategy) {
        //类型: 1火, 2水, ...
        var supTypeSel = '#prt-type > '+
            '.btn-type.icon-supporter-type-'+supType.toString();
        var supTypeSelDone = supTypeSel+'.selected';
        //列表
        var supListSel = '.prt-supporter-list > '+
            '.prt-supporter-attribute:not(.disableView)';
        var okSel = '.btn-usual-ok.se-quest-start'

        //选类型
        var chooseType = function() {
            return this.waitOnce(supTypeSel).then(function(){
                this.click(supTypeSel);
                return this.waitOnce(supTypeSelDone).then(function(){
                    return this.waitOnce(supListSel);
                }.bind(this));
            }.bind(this));
        }.bind(this);

        //选列表
        var chooseSummon = function() {
            var supList = $(supListSel);
            for (var i = 0; i < supStrategy.length; ++i) {
                var strategy = supStrategy[i];
                var summon = supList.children('.btn-supporter.lis-supporter[data-supporter-evolution="'+
                        strategy.summonEvo.toString()+'"]');
                summon = summon.has('.prt-supporter-info > .prt-summon-image[data-image="'+
                        strategy.summonImg.toString()+'"]');
                if (summon.size()>0) {
                    this.click(summon);
                    return this.waitOnce('.btn-usual-ok.se-quest-start');
                }
            }
            //没有符合要求的，选第一个
            var summon = supList.children().first();
            this.click(summon);
            return this.waitOnce(okSel);
        }.bind(this);

        //确认AP是否足够
        var checkAP = function() {
            if (Number($('.txt-stamina-after').text()) < 0) {
                return false;
            } else {
                return true;
            }
        }.bind(this);

        //进入战斗
        var enterQuest = function() {
            this.click(okSel);
            return this.sleep(2).then(function() {
                return Promise.race([this.waitUntilVisible('.prt-start-direction'),
                    this.sleep(10)]).then(function() {
                    return this.eachBattle();
                }.bind(this));
            }.bind(this));
            //return Promise.resolve();
        }.bind(this);

        this.gotoPage(this.QUEST);
        return this.sleep(1).then(function(){
            return chooseType();
        }.bind(this)).then(function(){
            return chooseSummon();
        }.bind(this)).then(function(){
            if (checkAP()) {
                return enterQuest();
            } else {
                return this.restoreAP(1, this.chooseSupporter.bind(
                        this, supType, supStrategy));
            }
        }.bind(this));
    },

    //检查是否有buff或debuff
    checkStatus: function(index, s) {
        var charaStatus = $('.prt-status.prt-condition[pos="'+index+'"]');
        if (charaStatus.find('[data-status="'+s.toString()+'"]').size()>0) {
            return true;
        } else {
            return false;
        }
    },
    //有分身??
    hasBarrier: function(index) {
        return this.checkStatus(index, 1003);
    },
    //检查是否受伤
    isInjured: function(index) {
        var injured = -1;
        var charaHp = $('.prt-gauge-hp:visible').eq(index);
        var total = charaHp.width();
        var current = charaHp.children('.prt-gauge-hp-inner').width();
        if (current / total < this.HEAL_THRES && !this.hasBarrier(index)) {
            return true;
        }
        return false;
    },
    //治疗
    heal: function(index) {
        if (!this.isInjured(index)) {
            return Promise.resolve();
        }
        this.click('.btn-temporary');
        return this.waitOnce('.pop-raid-item.pop-show').then(function() {
            var greenPotionCount = parseInt($('.lis-item .having-num').eq(0).text());
            var bluePotionCount = parseInt($('.lis-item .having-num').eq(1).text());
            var eventPotionCount = parseInt($('.lis-item .having-num').eq(3).text() || 0);
            var eventReviveCount = parseInt($('.lis-item .having-num').eq(5).text() || 0);
            this.greenPotionCount = greenPotionCount;
            this.bluePotionCount = bluePotionCount;
            this.eventReviveCount = eventReviveCount;
            this.eventPotionCount = eventPotionCount;

            if (greenPotionCount > 0) {
                this.click('.btn-temporary-small');
                return this.waitOnce('div.prt-popup-header:visible').then(function() {
                    this.click('.btn-command-character:visible:eq(' + index + ')');
                    this.greenPotionCount--;
                    return this.sleep(3);
                }.bind(this)).then(function() {
                    return this.actionable();
                }.bind(this));
            } else if (bluePotionCount > 0) {
                this.click('.item-large.btn-temporary-large');
                return this.waitOnce('div.prt-popup-header:visible').then(function() {
                    this.click('.btn-usual-use');
                    this.bluePotionCount--;
                    return this.sleep(3);
                }.bind(this)).then(function() {
                    return this.actionable();
                }.bind(this));
            } else {
                this.click('.pop-raid-item .btn-usual-cancel');
                return this.waitUntilInvisible('.pop-raid-item');
            }
        }.bind(this));
    },
    healIfInjured: function() {
        return this.heal(0).then(function() {
            return this.heal(1);
        }.bind(this)).then(function() {
            return this.heal(2);
        }.bind(this)).then(function() {
            return this.heal(3);
        }.bind(this)).then(function() {
            return this.actionable();
        }.bind(this));
    },
    //到下一波
    gotoNextWave: function() {
        this.click('.btn-result');
        return this.sleep(2).then(function() {
            return this.eachWave();
        }.bind(this));
    },

    //检查是否这波结束
    checkNextWave: function() {
        if ($('.btn-result').is(':visible')) {
            return true;
        } else {
            return false;
        }
    },
    //等待到继续行动
    actionable: function() {
        return this.waitUntilVisible('.btn-attack-start.display-on, .btn-result:visible').then(function() {
            //this.debug('ready for next action');
            return Promise.resolve();
        }.bind(this));
    },
    //获取波数
    getWaveNum: function() {
        var num = $('.txt-info-num:visible').children(':first').attr('class');
        return Number(num.replace('num-info', ''));
    },
    //获取最终波数
    getFinalWaveNum: function() {
        var num = $('.txt-info-num:visible').children(':last').attr('class');
        return Number(num.replace('num-info', ''));
    },
    //获取回合数
    getTurnNum: function() {
        var className = '';
        var turn = $('.prt-turn-info > .prt-number > div');
        turn.each(function() {
            className = $(this).attr('class') || className;
        });
        if (!className) {
            return 0;
        }
        return Number(className.replace('num-turn', ''));
    },
    checkChargeAtk: function(lock) {
        var btnSel = '.btn-lock';
        var open = false;
        /*
        if (lock) {
            if ($(btnSel).hasClass('lock0')) {
                this.click(btnSel);
            }
            return false;
        }
        */


        if (!lock &&
            ($(".prt-member .lis-character0 .prt-gauge-special-inner").attr('style') == "width: 100%;") &&
            ($(".prt-member .lis-character1 .prt-gauge-special-inner").attr('style').split(':')[1].replace(/%;/, "") >= 90) &&
            ($(".prt-member .lis-character2 .prt-gauge-special-inner").attr('style').split(':')[1].replace(/%;/, "") >= 80) &&
            ($(".prt-member .lis-character3 .prt-gauge-special-inner").attr('style').split(':')[1].replace(/%;/, "") >= 70)) {

            open = true
        }

        if (open) {
            if ($(btnSel).hasClass('lock1')) {
                this.click(btnSel);
            }
            return true;
        } else {
            if ($(btnSel).hasClass('lock0')) {
                this.click(btnSel);
            }
            return false;
        }
    },
    eachBattle: function() {
        this.wave = 0;
        this.finalWave = 99;
        return this.actionable().then(function() {
            return this.eachWave();
        }.bind(this)).then(function() {
            return Promise.resolve();
        }.bind(this));
    },
    waveChange: function() {
        var prevWave = this.wave || 0;
        return this.waitUntilVisible('.txt-info-num').then(function() {
            this.finalWave = this.getFinalWaveNum();
            var curWave = this.getWaveNum();
            if (prevWave == curWave) {
                return this.sleep(1).then(function() {
                    return this.waveChange();
                }.bind(this));
            } else {
                this.wave = curWave;
                return Promise.resolve();
            }
        }.bind(this));
    },
    //每波策略
    eachWave: function() {
        if (this.wave == this.finalWave) {
            return Promise.resolve();
        } else {
            return this.waveChange().then(function() {
                return this.actionable();
            }.bind(this)).then(function() {
                return this.eachTurn();
            }.bind(this));
        }
    },
    //每回合策略
    eachTurn: function() {
        var wave = this.getWaveNum();
        var turn = this.getTurnNum();
        this.checkChargeAtk(true);
        this.actionable().then(function() {
            if (wave == 1 || wave == 3) {
            //if (true) {
                return this.summon([5,0,1,2,3,4]);
            } else {
                return Promise.resolve();
            }
        }.bind(this)).then(function() {
            if (wave == 1) {
            //if (true) {
                return this.cast(0,[1,3]).then(function(){
                    return this.cast(1,[2]);
                }.bind(this));
            } else if (wave == 3) {
                return this.cast(3,[1,0,2]).then(function(){
                    return this.cast(0,[1,3]);
                }.bind(this)).then(function(){
                    return this.cast(1,[2]);
                }.bind(this)).then(function(){
                    return this.cast(2,[0,1]);
                }.bind(this));
            } else {
                return Promise.resolve();
            }
        }.bind(this)).then(function(){
            this.attack();
            return this.sleep(3);
        }.bind(this)).then(function(){
            return this.actionable();
        }.bind(this)).then(function(){
            if (this.checkNextWave()) {
                return this.gotoNextWave();
            } else {
                return this.eachTurn();
            }
            //return Promise.resolve();
        }.bind(this));
    },
    //点击攻击
    attack: function() {
        this.click('.btn-attack-start.display-on');
        //this.debug('attack, ' + this.turn);
    },
    //第i个人施放技能skills[]
    cast: function(i, skills) {
        var self = this;
        var skillState = $('.btn-command-character[pos="'+
                i.toString()+'"]:visible .lis-ability-state');

        //检查是否有可施放的技能
        var available = false;
        for (var sId of skills) {
            if (skillState.eq(sId).attr('state') === '2') {
                available = true;
                break;
            }
        }
        if (!available) {
            return Promise.resolve();
        }

        this.click('.btn-command-character[pos="'+
                i.toString()+'"]:visible');

        //施放第skillId个技能（如果能施放）
        var _cast = function(skillId) {
            //有可能这波已完
            if (self.checkNextWave()) {
                return Promise.resolve();
            }
            //alert('i='+i+'; skillId='+skillId);
            //var skillName = $('.lis-ability:visible').eq(skillId).find('[ability-name]').attr('ability-name');
            var skillSel = '.lis-ability:visible:eq('+skillId.toString()+')';
            if (!$(skillSel).is('.btn-ability-available')) {
                return Promise.resolve();
            }

            self.click(skillSel);
            return self.waitUntilVisible('.prt-log.log-ability').then(function() {
                return self.actionable().then(function() {
                    return self.waitUntilInvisible('.prt-log.log-ability');
                });
            });
        };
        
        var _doCast = function() {
            if (skills.length > 0) {
                var skillId = skills.shift();
                return _cast(skillId).then(function() {
                    return _doCast();
                });
            } else {
                self.click('.btn-command-back.display-on');
                return self.waitUntilInvisible('.prt-command-chara[pos="'+(i+1).toString()+'"]');
            }
        };

        return this.sleep(1).then(function() {
            return this.waitUntilVisible('.prt-ability-list')
        }.bind(this)).then(function() {
            return _doCast();
        }.bind(this)).then(function() {
            return this.actionable();
        }.bind(this));
    },

    //召唤
    summon: function(summons) {
        if ($('.summon-on').length <= 0) {
            return Promise.resolve();
        }

        //检查是否有需要使用召唤
        var summonPos = null;
        for (var sId of summons) {
            var summonSel = '.lis-summon[pos="'+(sId+1).toString()+'"]';
            if ($(summonSel).is('.on.btn-summon-available')) {
                summonPos = sId+1;
                break;
            }
        }
        if (summonPos == null) {
            return Promise.resolve();
        }

        this.click('.summon-on');
        return this.waitOnce('.summon-show').then(function() {
            return this.sleep(1);
        }.bind(this)).then(function() {
            var summonSel = '.lis-summon[pos="'+summonPos.toString()+'"]';
            this.click(summonSel);
            return this.waitUntilVisible('.btn-summon-use');
        }.bind(this)).then(function() {
            this.click('.btn-summon-use');
            return this.sleep(1);
        }.bind(this)).then(function() {
            return this.waitUntilInvisible('.btn-summon-use');
        }.bind(this)).then(function() {
            return this.actionable();
        }.bind(this));
    },

    /*
    observeWaveInfo: function() {
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            this.waitUntilVisible('.btn-attack-start.display-on').then(function() {
                this.eachWave(this.getWaveNum());
            }.bind(this));
        }.bind(this));

        observer.observe(document.querySelector('.prt-battle-num'), {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        //this.observers.push(observer);
    },
    observeEnd: function() {
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            if ($('.btn-result').is(':visible')) {
                this.click('.btn-result');
            }
        }.bind(this));

        observer.observe(document.querySelector('.prt-command-end'), {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        //this.observers.push(observer);
    }
    */
    /*
    stop: function() {
        if (!this._enabled) {
            return;
        }
        console.log('client paused');
        this._enabled = false;
    }*/
    start: function() {
        return this.chooseSupporter(this.SUP_TYPE, this.SUP_STRATEGY);
    }
};

function main() {
    autoBattle.start();
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ main +')();'));
script.id = 'orzorzorz';
(document.head || document.body || document.documentElement).appendChild(script);
