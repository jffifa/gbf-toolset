/*
 (c) Higehito Higeyama , since 2016
*/
var popStatus={targetElm:null,ini:function(){(this.targetElm=document.getElementById("pop-scroll"))&&(new MutationObserver(this.observeHandler)).observe(this.targetElm,{childList:!0,subtree:!0})},observeHandler:function(d,g){if(2==d.length){var a=d[0].target;if("pop-condition"==a.className&&(a=a.querySelectorAll(".lis-conditon"),0<a.length)){for(var f=function(a){var c=a.querySelector(".prt-icon > img"),b={name:null,detail:null,remain:null,turn:null};if(c){if(c=c.src.match(/\/status_([\d_]*)/))b.status=
c[1];if(c=a.querySelector(".prt-detail > .txt-explain"))b.detail=c.textContent;if(c=a.querySelector(".time"))b.remain=c.textContent;if(a=a.querySelector(".turn"))b.turn=a.textContent;return b}},e={buff:[],debuff:[]},b=0;b<a.length;b++)-1!=a[b].className.indexOf("lis-conditon buff")?e.buff.push(f(a[b])):-1!=a[b].className.indexOf("lis-conditon debuff")&&e.debuff.push(f(a[b]));popStatus.requestCreateSubWindow(e)}}},requestCreateSubWindow:function(d){chrome.extension.sendRequest({status:"subwindowopen",
data:d},function(){})},convMemberData:function(){}};
