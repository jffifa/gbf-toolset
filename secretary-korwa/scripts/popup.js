'use strict';

var Popup = {
  start: function() {
    this.initSkills();
    this.initQuests();
    //set triggers
    $('#quest-ok').on('click', function() {
      $('#quest-id').trigger('change');
    });
    $('#quest-cancel').on('click', function() {
      $('#quest-id').val('').trigger('change');
    });
    $('#coop-ok').on('click', function() {
      $('#coop-id').trigger('change');
    });
    $('#coop-cancel').on('click', function() {
      $('#coop-id').val('').trigger('change');
    });
    $('#summon-ok').on('click', function() {
      $('#select-id').trigger('change');
    });	

    //set storage
    $('form').on('click', '[data-quest-id]', function() {
      $('#quest-id').val($(this)[0].dataset.questId).trigger('change');
    });
    $('form').on('change', 'input[type=checkbox]', function() {
      console.log($(this).attr('name'), $(this).is(":checked"));
      window.localStorage.setItem($(this).attr('name'), $(this).is(":checked"));
    });
    $('form').on('change', 'input[type=text]', function() {
      console.log($(this).attr('name'), $(this).val());
      window.localStorage.setItem($(this).attr('name'), $(this).val());
    });
    $('form').on('change', 'select', function() {
      console.log($(this).attr('name'), $(this).val());
      window.localStorage.setItem($(this).attr('name'), $(this).val());
    });
    $('form').on('click', 'input[type=radio]', function() {
      console.log($(this).attr('name'), $(this).val());
      window.localStorage.setItem($(this).attr('name'), $(this).val());
    })
    
    //set value
    $('#quest-id').val(window.localStorage.getItem('quest-id'));
    $('#coop-id').val(window.localStorage.getItem('coop-id'));
    $('#select-id').val(window.localStorage.getItem('select-id'));
    $('#gear-rate').val(window.localStorage.getItem('gear-rate'));
    $('form input[type=checkbox]').each(function() {
      if (window.localStorage.getItem($(this).attr('name')) === 'true') {
        $(this).attr('checked', true);
      }
    });
    $('form select').each(function() {
      var _v = window.localStorage.getItem($(this).attr('name'));
      if (_v !== null) {
        $(this).children('option[value="'+_v+'"]').prop('selected', true);
      }
    });
    $('form input[type=radio]').each(function() {
      var _v = window.localStorage.getItem($(this).attr('name'));
      if (_v === $(this).val()) {
        $(this).prop('checked', true);
      }
    });
  },
  initQuests: function() {
    var quests = window.localStorage.getItem('quest-database');
    if (!quests) {
      return;
    }
    quests = JSON.parse(quests);
    for (var id in quests) {
      if (quests[id].hash) {
        $('#quest-history').append('<button class="btn btn-default" type="button" data-quest-id="'+quests[id].hash+'"><img src="'+quests[id].thumb+'" />'+(quests[id].questName || quests[id].hash)+'</button')
      }
    }
  },
  initSkills: function() {
    var container = document.getElementById('skill-list');
    chrome.extension.sendRequest({type: 'request_abilities'}, function(abilities) {
      abilities.forEach(function(ability) {
        var charPos = Number(ability.position[0]);
        var skillPos = Number(ability.position[1]);
        if (!$('div[pos="'+charPos+'"]').length) {
          $(container).append('<div pos="'+charPos+'"></div>');
        }
        var checked = '';
        if (window.localStorage.getItem(ability.name) !== 'false') {
          checked = 'checked';
        }
        $('div[pos="'+charPos+'"]').append('<span><input type="checkbox" name="'+ability.name+'" '+checked+' /><img alt="'+ability.name+':'+ability.text+'" src="http://gbf.game-a.mbga.jp/assets/img/sp/ui/icon/ability/m/'+ability.icon+'.png" /></span>');
      });
    });
  }
};
document.addEventListener('DOMContentLoaded', function () {
  Popup.start();
});


