'use strict';

define(function() {
    return function(button) {
        if ($(button)) {
            $(button).trigger('mousedown');
            $(button).trigger('tap');
        } else {
            console.log('button selector ['+button+'] not found');
        }
    }
});