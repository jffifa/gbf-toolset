'use strict';

window.SkillChecker = {
    'ディレイ': function() {}
};

window.Engine = {
    HEAL_THRES: 0.5,
    start: function() {
        console.log('Engine Start!!');
        this.observers = [];
        this.observeEnd();
        if (window.location.hash.indexOf('raid_multi') >= 0) {
            this._handle_raid_multi();
        } else {
            this._handle_raid();
        }
    },
    '_handle_raid_multi': function() {
        console.log('inject complete!!');
        Promise.race([
            this.waitUntilVisible('div.prt-popup-header:contains("救援依頼")'),
            this.sleep(5)
        ]).then(function() {
            this.click('.btn-usual-cancel');
            return this.sleep(3);
        }.bind(this)).then(function() {
            this.eachTurn(0);
        }.bind(this));
    },
    '_handle_raid': function() {
        this.observeWaveInfo();
        this.observeEnd();
        // this.observePop();
    },
    debug: function(msg) {
        window.dispatchEvent(new CustomEvent('_debug', {
            detail: arguments
        }))
    },
    isMultiRaid: function() {
        return window.location.hash.indexOf('raid_multi') >= 0;
    },
    /*
    getFreePotion: function() {
        if (this.isMultiRaid()) {
            return this.waitUntilVisible('....');
        } else {
            return Promise.resolve();
        }
    },
    */
    yodarhaMode: function() {
        var pos = window.localStorage.getItem('yodarha-mode');
        if (pos) {
            if (pos === '0') {
                return 0;
            } else {
                return Number(pos) - 1;
            }
        } else {
            return 0;
        }
    },
    yodarhaCharge: function() {
        var yodarhaPos = this.yodarhaMode();
        return this.checkStatus(yodarhaPos, 14143);
    },
    //检查是否这波结束
    checkNextWave: function() {
        return $('.btn-result').is(':visible');
    },
    //获取波数
    getWaveNum: function() {
        var num = $('.txt-info-num:visible').children(':first').attr('class');
        if (num) {
            return Number(num.replace('num-info', ''));
        } else {
            return 0;
        }
    },
    //获取最终波数
    getFinalWaveNum: function() {
        var num = $('.txt-info-num:visible').children(':last').attr('class');
        if (num) {
            return Number(num.replace('num-info', ''));
        } else {
            return 99;
        }
    },
    //获取回合数
    getTurnNum: function() {
        var className = '';
        var turn = $('.prt-turn-info .prt-number div');
        turn.each(function() {
            className = $(this).attr('class') || className;
        });
        if (!className) {
            return 0;
        }
        return Number(className.replace('num-turn', ''));
    },
    click: function(selector) {
        window.localStorage.setItem('button-selector', selector);
        this.sleep(0.5).then(function() {
            if ($(selector).size()>0) {
                $(selector).trigger('mousedown');
                $(selector).trigger('tap');
            }
        }.bind(this));
        this.debug('click:', selector);
    },
    eachWave: function(wave) {
        this.wave = wave;
        this.greenPotionCount = undefined;
        this.bluePotionCount = undefined;
        this.eventHealCount = undefined;
        this.eventReviveCount = undefined;
        if (this.turn === undefined) {
            this.eachTurn(0);
        }
    },
    //某个角色能否奥义攻击
    canChargeAtk: function(index) {
        var sel = ".prt-member .lis-character"+index.toString()+" .prt-gauge-special-inner";
        var charge = Number($(sel).attr('style').split(':')[1].replace(/%;/, ""));
        var prevCharge = 0;
        for (var i = 0; i < index; ++i){
            if (this.canChargeAtk(i)) {
                prevCharge++;
            }
        }
        return (charge >= 100-10*prevCharge);
    },
    checkChargeAtk: function(lock) {
        if (lock === undefined) {
            lock = false;
        }
        var btnSel = '.btn-lock';
        var open = false;

        var yodarhaPos = this.yodarhaMode();
        if (yodarhaPos) {
            if (this.isBossWave() &&
                this.yodarhaCharge() && this.canChargeAtk(yodarhaPos)) {
                open = true;
            }
        } else {
            if (!lock &&
                this.canChargeAtk(0) && this.canChargeAtk(1) && this.canChargeAtk(2) && this.canChargeAtk(3)) {
                open = true
            }
        }

        if (open) {
            if ($(btnSel).hasClass('lock1')) {
                this.click(btnSel);
            }
            return true;
        } else {
            if ($(btnSel).hasClass('lock0')) {
                this.click(btnSel);
            }
            return false;
        }
    },
    //每一轮策略
    eachTurn: function(turn) {
        if (this.turn === turn) {
            return;
        }
        this.turn = turn;
        var currentWave = this.wave;

        this.attackable().then(function() {
            //治疗
            return this.healIfInjured();
        }.bind(this)).then(function() {
            //召唤
            if (this.isBossWave()) {
                return this.summonIfPossible();
            } else {
                return Promise.resolve();
            }
        }.bind(this)).then(function() {
            //老头模式
            var yodarhaPos = this.yodarhaMode();
            if (yodarhaPos && this.isBossWave()) {
                if (this.canChargeAtk(yodarhaPos) && !this.yodarhaCharge()) {
                    return this.castSkills(yodarhaPos, [2]);
                }
            } else {
                return Promise.resolve();
            }
        }.bind(this)).then(function() {
            //释放技能
            if (this.isBossWave()) {
                return this.castIfAvailable();
            } else {
                return Promise.resolve();
            }
        }.bind(this)).then(function() {
            //奥义
            this.checkChargeAtk();
            return Promise.resolve();
        }.bind(this)).then(function() {
            this.attack();
            return this.sleep(3);
        }.bind(this)).then(function() {
            return this.attackable();
        }.bind(this)).then(function() {
            if (this.wave !== currentWave) {
                return Promise.resolve();
            }
            this.eachTurn(this.getTurnNum());
        }.bind(this));
    },
    observeEnd: function() {
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            if ($('.btn-result').is(':visible')) {
                this.click('.btn-result');
            }
        }.bind(this));

        observer.observe(document.querySelector('.prt-command-end'), {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        this.observers.push(observer);
    },
    observePop: function() {
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            if ($('.btn-usual-ok:visible').length) {
                this.click('.btn-usual-ok');
                observer.disconnect();
            } else if ($('.btn-usual-cancel:visible').length) {
                this.click('.btn-usual-cancel');
                observer.disconnect();
            } else if ($('.btn-usual-close:visible').length) {
                this.click('.btn-usual-close');
                observer.disconnect();
            }
        }.bind(this));

        observer.observe(document.querySelector('#pop'), {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        this.observers.push(observer);
    },
    observeTurnInfo: function() {
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.attributeName === 'class' && $('.prt-turn-info.anim-on').length) {
                    var turn = Number($('.prt-turn-info .prt-number').children().eq(0).attr('class').replace('num-turn', ''));
                    this.eachTurn(turn);
                }
            }, this);
        }.bind(this));

        observer.observe(document.querySelector('.prt-turn-info'), {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        this.observers.push(observer);
    },
    observeWaveInfo: function() {
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            this.waitUntilVisible('.btn-attack-start.display-on').then(function() {
                this.eachWave(Number($('.txt-info-num:visible').children(':first').attr('class').replace('num-info', '')));
            }.bind(this));
        }.bind(this));

        observer.observe(document.querySelector('.prt-battle-num'), {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        this.observers.push(observer);
    },
    waitUntilVisible: function(selector) {
        this.debug('waiting visible:' + selector);
        return new Promise(function(resolve) {
            if ($(selector).is(":visible")) {
                resolve();
                return;
            }
            this.visibleObserver && this.visibleObserver.disconnect();
            var self = this;
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if ($(selector).is(":visible")) {
                        this.visibleObserver && this.visibleObserver.disconnect();
                        resolve();
                    }
                }, this);
            }.bind(this));

            observer.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: true,
                characterData: false
            });
            this.visibleObserver = observer;
        }.bind(this));
    },
    waitUntilInvisible: function(selector) {
        return new Promise(function(resolve) {
            if (!$(selector).is(":visible")) {
                resolve();
                return;
            }
            this.invisibleObserver && this.invisibleObserver.disconnect();
            var self = this;
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if (!$(selector).is(":visible")) {
                        this.invisibleObserver && this.invisibleObserver.disconnect();
                        resolve();
                    }
                }, this);
            }.bind(this));

            observer.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: true,
                characterData: false
            });
            this.invisibleObserver = observer;
        }.bind(this));
    },
    waitOnce: function(selector) {
        this.debug('waiting:' + selector);
        return new Promise(function(resolve) {
            this.onceObserver && this.onceObserver.disconnect();
            var self = this;
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if ($(selector).length || document.querySelector(selector)) {
                        this.onceObserver && this.onceObserver.disconnect();
                        resolve();
                    }
                }, this);
            }.bind(this));

            observer.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: true,
                characterData: false
            });
            this.onceObserver = observer;
        }.bind(this));
    },
    change: function(callback, dom) {
        this.observer && this.observer.disconnect();
        var self = this;
        var observer = new MutationObserver(function(mutations) {
            callback(mutations);
        }.bind(this));

        observer.observe(dom || document.body, {
            childList: true,
            subtree: true,
            attributes: true,
            characterData: false
        });
        this.observer = observer;
    },
    stop: function() {
        this.observer && this.observer.disconnect();
    },
    isBossWave: function() {
        return (window.location.hash.indexOf('raid_multi') >= 0) ||
            (this.getWaveNum() === this.getFinalWaveNum());
    },
    /**
     * Return the npc id who is injured.
     */
    isInjured: function(index) {
        var injured = -1;
        var npchp = $('.prt-gauge-hp:visible').eq(index);
        var total = npchp.width();
        var current = npchp.children('.prt-gauge-hp-inner').width();
        return (current / total < this.HEAL_THRES && !this.hasBarrier(index));
    },
    //检查是否有buff或debuff
    checkStatus: function(index, s) {
        var charaStatus = $('.prt-status.prt-condition[pos="'+index+'"]');
        return charaStatus.find('[data-status="'+s.toString()+'"]').size()>0;
    },
    //有分身??
    hasBarrier: function(index) {
        return this.checkStatus(index, 1003);
    },
    //等待到能够攻击或者下一轮
    attackable: function() {
        return this.waitUntilVisible('.btn-attack-start.display-on').then(function() {
            this.debug('ready for next attack');
            return Promise.resolve();
        }.bind(this));
    },
    //等待到能够攻击或者下一轮
    actionable: function() {
        return this.waitUntilVisible('.btn-attack-start.display-on, .btn-result:visible').then(function() {
            this.debug('ready for next action');
            return Promise.resolve();
        }.bind(this));
    },
    healIfInjured: function() {
        return this.heal(0).then(function() {
            return this.heal(1);
        }.bind(this)).then(function() {
            return this.heal(2);
        }.bind(this)).then(function() {
            return this.heal(3);
        }.bind(this));
    },
    castIfAvailable: function() {
        if (window.localStorage.getItem('normal-attack-only') === 'true') {
            return Promise.resolve();
        }
        return this.castAllSkill(0).then(function() {
            return this.castAllSkill(1);
        }.bind(this)).then(function() {
            return this.castAllSkill(2);
        }.bind(this)).then(function() {
            return this.castAllSkill(3);
        }.bind(this));
    },
    summonIfPossible: function() {
        if ($('.summon-on').length && window.localStorage.getItem('auto-summon') === 'true') {
            return this.summon()
        } else {
            Promise.resolve();
        }
    },
    summon: function() {
        if ($('.summon-on').length <= 0) {
            return Promise.resolve();
        }
        
        this.click('.summon-on');
        return this.waitOnce('.summon-show').then(function() {
            return this.sleep(1.5);
        }.bind(this)).then(function() {
            this.click('.lis-summon.on.btn-summon-available:first');
            return this.waitUntilVisible('.btn-summon-use');
        }.bind(this)).then(function() {
            this.click('.btn-summon-use');
            return this.sleep(1.5);
        }.bind(this)).then(function() {
            return this.waitUntilInvisible('.btn-summon-use');
        }.bind(this)).then(function() {
            return this.attackable();
        }.bind(this));
    },
    getSkillPreference: function(skill) {
        return window.localStorage.getItem(skill) !== 'false';
    },
    notFirstTurnSkill: function(skillName) {
        var set = ['ディレイ', 'ヴォーパルレイジ'];
        return set.some(function(match) {
            return (skillName.indexOf(match) >= 0);
        });
    },
    //第i个人施放技能skills[]
    castSkills: function(i, skills) {
        var self = this;
        var skillState = $('.btn-command-character[pos="'+
                i.toString()+'"]:visible .lis-ability-state');

        //检查是否有可施放的技能
        var available = false;
        for (var sId of skills) {
            if (skillState.eq(sId).attr('state') === '2') {
                available = true;
                break;
            }
        }
        if (!available) {
            return Promise.resolve();
        }

        this.click('.btn-command-character[pos="'+
                i.toString()+'"]:visible');

        //施放第skillId个技能（如果能施放）
        var _cast = function(skillId) {
            //有可能这波已完
            if (self.checkNextWave()) {
                return Promise.resolve();
            }
            var skillSel = '.lis-ability:visible:eq('+skillId.toString()+')';
            if (!$(skillSel).is('.btn-ability-available')) {
                return Promise.resolve();
            }

            self.click(skillSel);
            return self.waitUntilVisible('.prt-raid-log.log-ability').then(function() {
                return self.actionable();
            }).then(function() {
                return self.waitUntilInvisible('.prt-raid-log.log-ability');
            });
        };
        
        var _doCast = function() {
            if (skills.length > 0) {
                var skillId = skills.shift();
                return _cast(skillId).then(function() {
                    return _doCast();
                });
            } else {
                self.click('.btn-command-back.display-on');
                return self.waitUntilInvisible('.prt-command-chara[pos="'+(i+1).toString()+'"]');
            }
        };

        return this.sleep(1).then(function() {
            return this.waitUntilVisible('.prt-ability-list')
        }.bind(this)).then(function() {
            return _doCast();
        }.bind(this)).then(function() {
            return this.actionable();
        }.bind(this));
    },
    cast: function(skillId) {
        var skillName = $('.lis-ability:visible:eq('+skillId+')').find('[ability-name]').attr('ability-name');
        if ((skillName && (this.getTurnNum() === 0 && this.notFirstTurnSkill(skillName))) ||
            this.getSkillPreference(skillName) === false ||
            !$('.lis-ability:visible:eq('+skillId+')').is('.btn-ability-available')) {
            return Promise.resolve();
        } else {
            this.click('.lis-ability:visible:eq('+skillId+')');
            return this.waitUntilVisible('.prt-raid-log.log-ability').then(function() {
                return this.attackable();
            }.bind(this)).then(function() {
                return this.waitUntilInvisible('.prt-raid-log.log-ability');
            }.bind(this));
        }
    },
    castAllSkill: function(npcId) {
        var self = this;
        var states = $('.btn-command-character[pos="'+npcId+'"]:visible .lis-ability-state');
        var available = false;
        states.each(function(index) {
            var skillName = $('.prt-ability-list').eq(npcId).find('[ability-name]').eq(index).attr('ability-name');
            if (self.getSkillPreference(skillName) === false ||
                self.getTurnNum() === 0 && skillName && skillName.indexOf('ディレイ') >= 0) {
                    return true;
                }
            if ($(this).attr('state') === '2') {
                available = true;
                return false;
            }
        });
        if (!available) {
            return Promise.resolve();
        }
        this.click('.btn-command-character[pos="'+npcId+'"]:visible');
        return this.sleep(1).then(function() {
            return this.waitUntilVisible('.prt-ability-list')
        }.bind(this)).then(function() {
            return this.cast(0);
        }.bind(this)).then(function() {
            return this.cast(1);
        }.bind(this)).then(function() {
            return this.cast(2);
        }.bind(this)).then(function() {
            return this.cast(3);
        }.bind(this)).then(function() {
            this.click('.btn-command-back.display-on');
            return this.waitUntilInvisible('.prt-command-chara[pos="'+(npcId+1)+'"]')
        }.bind(this));
    },
    /*
    waitForTransitionend: function(selector) {
        return new Promise(function(resolve) {
            $(selector).one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
                resolve();
            });
        });
    },
    */
    noPotion: function() {
        return this.greenPotionCount === 0 && this.bluePotionCount === 0 &&
            (window.localStorage.getItem('use-event-potion') !== 'true' ||
             this.eventPotionCount === 0) &&
            (window.localStorage.getItem('use-event-revive') !== 'true' ||
             this.eventReviveCount === 0);
    },
    //治疗某个角色
    heal: function(index) {
        if (!this.isInjured(index) || this.noPotion()) {
            return Promise.resolve();
        }
        this.click('.btn-temporary');
        return this.waitOnce('.pop-raid-item.pop-show').then(function() {
            var greenPotionCount = parseInt($('.lis-item .having-num').eq(0).text());
            var bluePotionCount = parseInt($('.lis-item .having-num').eq(1).text());
            var eventPotionCount = parseInt($('.lis-item .having-num').eq(3).text() || 0);
            var eventReviveCount = parseInt($('.lis-item .having-num').eq(5).text() || 0);
            this.greenPotionCount = greenPotionCount;
            this.bluePotionCount = bluePotionCount;
            this.eventReviveCount = eventReviveCount;
            this.eventPotionCount = eventPotionCount;

            if (greenPotionCount > 0) {
                this.click('.btn-temporary-small');
                return this.waitOnce('div.prt-popup-header:visible').then(function() {
                    this.click('.btn-command-character:visible:eq(' + index + ')');
                    this.greenPotionCount--;
                    return this.sleep(3);
                }.bind(this)).then(function() {
                    return this.attackable();
                }.bind(this));
            } else if (bluePotionCount > 0) {
                this.click('.item-large.btn-temporary-large');
                return this.waitOnce('div.prt-popup-header:visible').then(function() {
                    this.click('.btn-usual-use');
                    this.bluePotionCount--;
                    return this.sleep(3);
                }.bind(this)).then(function() {
                    return this.attackable();
                }.bind(this));
            } else if (window.localStorage.getItem('use-event-potion') === 'true' && eventPotionCount > 0) {
                this.click('.lis-item.btn-event-item[item-id="1"]');
                return this.waitOnce('div.prt-popup-header:visible').then(function() {
                    this.click('.btn-usual-ok');
                    this.eventPotionCount--;
                    return this.sleep(3);
                }.bind(this)).then(function() {
                    return this.attackable();
                }.bind(this));
            } else if (window.localStorage.getItem('use-event-revive') === 'true' && eventReviveCount > 0) {
                this.click('.lis-item.btn-event-item[item-id="3"]');
                return this.waitOnce('div.prt-popup-header:visible').then(function() {
                    this.click('.btn-usual-ok');
                    this.eventPotionCount--;
                    return this.sleep(3);
                }.bind(this)).then(function() {
                    return this.attackable();
                }.bind(this));
            } else {
                this.click('.pop-raid-item .btn-usual-cancel');
                return this.waitUntilInvisible('.pop-raid-item');
            }
        }.bind(this));
    },
    //普通攻击
    attack: function() {
        this.click('.btn-attack-start.display-on');
        this.debug('attack, ' + this.turn);
    },
    sleep: function(sec) {
        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve();
            }, sec * 1000)
        }.bind(this));
    }
};

window.Engine.start();
