'use strict';

window.jsgear = {
    getGear: function(fnST, fnCT, fnSI, fnCI, fnDt, global) {

        var iRate = 1;
        var iFreeFrame = 9e9;
        var iLast, iTick;

        var aQueue = [];
        var nQueue = 1;
        var iFlag = 0;

        function addQueue(code, delay, arg, repeat) {
            if (!code) {
                return;
            }

            // 参数类型验证
            delay = +delay || 0;

            if (delay < 1) {
                delay = 1;
            }

            // 添加任务队列
            aQueue[nQueue] =
            {code:code, delay:delay, arg:arg, repeat:repeat, sum:0, flag:iFlag};

            return nQueue++;
        }

        function delQueue(id) {
            if (id >= 0) {
                delete aQueue[id];
            }
        }

        function hook() {
            global.setTimeout = setTimeout;
            global.clearTimeout = clearTimeout;
            global.setInterval = setInterval;
            global.clearInterval = clearInterval;
            global.Date = Date;

            for (var i = 0; i < requestFrameName.length; i++) {
                global[requestFrameName[i]] = requestAnimationFrame;
                global[cancelFrameName[i]] = clearTimeout;
            }

            iLast = iTick = +new fnDt;
            fnSI(onTimer, 1);
        }

        function unhook() {
            global.setTimeout = fnST;
            global.clearTimeout = fnCT;
            global.setInterval = fnSI;
            global.clearInterval = fnCI;
            global.Date = fnDt;

            for (var i = 0; i < requestFrameName.length; i++) {
                var k = requestFrameName[i];
                global[k] = requestFrameFn[k];
            }
        }

        function execute(task) {
            var code = task.code;
            task.arg ? code.apply(global, task.arg) : code();
        }


        function onTimer() {
            var cur = +new fnDt;
            var elapse = (cur - iLast) * iRate;


            for(var k in aQueue) {
                var task = aQueue[k];

                // 防止ie浏览器枚举时递归
                if (task.flag == iFlag) {
                    continue;
                }

                // 计时器累加
                task.sum += elapse;

                if (task.repeat) {		// setInterval
                    // 跳帧数
                    var skip = (task.sum / task.delay) >> 0;

                    // 最大跳帧数，防止卡死
                    if (skip > 32) {
                        skip = 32;
                    }

                    // 执行每一帧
                    while (--skip >= 0) {
                        execute(task);
                    }

                    // 剩余点数
                    task.sum %= task.delay;
                }
                else {				// setTimeout
                    if (task.sum >= task.delay) {
                        execute(task);

                        delete aQueue[k];
                        continue;
                    }
                }
            }

            iLast = cur;
            iTick += elapse;
            iFlag++;
        }


        /** 重定义时间函数 ***********************************/
        var SLICE = [].slice;

        function setTimeout(code, delay, arg) {
            if (arg) {
                arg = SLICE.call(arguments, 2);
            }
            return addQueue(code, delay, arg, false);
        }

        function clearTimeout(id) {
            delQueue(id);
        }

        function setInterval(code, delay, arg) {
            if (arg) {
                arg = SLICE.call(arguments, 2);
            }
            return addQueue(code, delay, arg, true);
        }

        function clearInterval(id) {
            delQueue(id);
        }

        function requestAnimationFrame(cb) {
            return setTimeout(cb, 16);
        }


        var requestFrameName = [],
            requestFrameFn = {},
            cancelFrameName = [],
            cancelFrameFn = {};



        var REQUEST_FRAME = [
            'oRequestAnimationFrame',
            'mozRequestAnimationFrame',
            'webkitRequestAnimationFrame',
            'msRequestAnimationFrame',
            'requestAnimationFrame'
        ];

        var CANCEL_FRAME = [
            'cancelAnimationFrame',
            'cancelRequestAnimationFrame',
            'mozCancelAnimationFrame',
            'mozCancelRequestAnimationFrame',
            'webkitCancelAnimationFrame',
            'webkitCancelRequestAnimationFrame',
            'oCancelAnimationFrame',
            'oCancelRequestAnimationFrame',
            'msCancelAnimationFrame',
            'msCancelRequestAnimationFrame'
        ];

        for(var i = REQUEST_FRAME.length - 1; i >= 0; i--) {
            var k = REQUEST_FRAME[i];
            if (global[k]) {
                requestFrameName.push(k);
                requestFrameFn[k] = global[k];
            }

            k = CANCEL_FRAME[i];
            if (global[k]) {
                cancelFrameName.push(k);
                cancelFrameFn[k] = global[k];
            }
        }


        // ==================================================
        // 重定义 Date
        // ==================================================
        function Date(y, m, d, h, min, s, ms) {
            if (this instanceof Date) {		// new Date(...)
                switch(arguments.length) {
                    case 0:
                        var cur = +new fnDt;
                        iTick += (cur - iLast) * iRate;
                        iLast = cur;
                        return new fnDt(iTick);

                    case 1: return new fnDt(y);
                    case 2:	return new fnDt(y, m);
                    case 3:	return new fnDt(y, m, d);
                    case 4:	return new fnDt(y, m, d, h);
                    case 5:	return new fnDt(y, m, d, h, min);
                    case 6:	return new fnDt(y, m, d, h, min, s);
                    default:return new fnDt(y, m, d, h, min, s, ms);
                }
            } else {							// Date()
                return new Date().toString();
            }
        }


        if (fnDt.now) Date.now = function() {
            var cur = fnDt.now();
            iTick += (cur - iLast) * iRate;
            iLast = cur;
            return Math.round(iTick);
        };

        Date.UTC = fnDt.UTC;
        Date.parse = fnDt.parse;
        Date.prototype = fnDt.prototype;

        // ==================================================
        // 导出接口
        // ==================================================

        function setup() {
            if (!enabled()) {
                hook();
            }
        }

        function unsetup() {
            if (enabled()) {
                unhook();
            }
        }

        function setRate(rate) {
            console.log('begin to set rate')
            iRate = rate;
        }

        function getRate() {
            return iRate;
        }

        function pause() {
            iFreeFrame = 0;
        }

        function resume() {
            iFreeFrame = 9e9;
        }

        function next(count) {
            iFreeFrame = count || 1;
        }
        
        function enabled() {
            return global.setTimeout != fnST;
        }

        return {
            enabled: enabled,
            setup: setup,
            unsetup: unsetup,
            setRate: setRate,
            getRate: getRate,
            pause: pause,
            resume: resume,
            next: next/*,

            rawSetTimeout: function() {return fnST.apply(global, arguments)},
            rawClearTimeout: function() {return fnCT.apply(global, arguments)},
            rawSetInterval: function() {return fnSI.apply(global, arguments)},
            rawClearInterval: function() {return fnCI.apply(global, arguments)}
            */
        };
    },
    init: function() {
        if (!window._jsgear) {
            window._jsgear = this.getGear(
                setTimeout,
                clearTimeout,
                setInterval,
                clearInterval,
                Date,
                window
            );
        }
    },
    setRate:function() {
        console.log('gear injection succeeded')
        this.init();
        if (window.localStorage.getItem('useGear') === 'true') {
            console.log('ready to gear');
            window._jsgear.setup();
            var rate = Number(window.localStorage.getItem('gear-rate'));
            if (!rate) {
                rate = 1.0;
            }
            window._jsgear.setRate(rate);
        } else {
            window._jsgear.unsetup();
        }
    }
};

window.jsgear.setRate();
