'use strict';

var inject = function(file, callback) {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = chrome.extension.getURL(file);
    s.onload = function() {
        this.parentNode.removeChild(this);
        callback && callback();
    };
    (document.head||document.documentElement).appendChild(s);
};
