//'use strict';

var kotoriAutoBattle = function() {
    var _isEnabled = true;
    var sleep(t) = function(){
    var click = function(jq){
        setTimeout(function(){
            var _ = jq;
            if (typeof jq == 'function') {
                _ = jq();
            } else if (typeof jq == 'string') {
                _ = $(jq);
            }
            if (_isEnabled && _.size()>0) {
                _.first().trigger('tap');
            } else {
                click(jq);
            }
        }, 1000+Math.random()*1000);
    },
        nth = function(selector, n){return $(selector).eq(n)};

    //进入选支援
    var AP = '#quest/extra/event';
    var QUEST = '#quest/supporter/710081/5';
    var gotoQuest = function(){window.location.href = QUEST};

    //选支援
    var SUP_STRATEGY = [
        {
            summonImg:'2040006000', //召唤石头像编号(80)
            summonEvo:'3' //召唤石突破等级
        },
        {
            summonImg:'2040042000', //召唤石头像编号(nezha)
            summonEvo:'3' //召唤石突破等级
        },
        {
            summonImg:'2040020000', //召唤石头像编号(bitch of wind)
            summonEvo:'3' //召唤石突破等级
        },
        {
            summonImg:'2040006000', //召唤石头像编号
            summonEvo:'2' //召唤石突破等级
        },
        {
            summonImg:'2040042000', //召唤石头像编号
            summonEvo:'2' //召唤石突破等级
        },
        {
            summonImg:'2040020000', //召唤石头像编号
            summonEvo:'2' //召唤石突破等级
        }
    ];
    var chooseSupporter = function(supType, supStrategy){
        //选类型: 1火, 2水, ...
        var chType = function(){
            var _t = '#prt-type > '+
                    '.btn-type.icon-supporter-type-'+supType.toString();
            click(_t);
        };
        var chSup = function(){
            var supList = $('.prt-supporter-list > .prt-supporter-attribute')
                .not('.disableView');
            for (i = 0; i < supStrategy.length; i++) {
                var strategy = supStrategy[i];
                var sup = supList.children('.btn-supporter.lis-supporter[data-supporter-evolution="'+strategy.summonEvo.toString()+'"]');
                sup = sup.has('.prt-supporter-info > .prt-summon-image[data-image="'+strategy.summonImg.toString()+'"]');
                if (sup.size()>0) {
                    click(sup);
                    return true;
                }
            }
            var sup = supList.children().first();
            click(sup);
            return false;
        };
        var clickOK = function(){
            var ok = '.pop-deck.supporter > '+
                    '.prt-btn-deck > '+
                    '.btn-usual-ok';
            click(ok);
            return true;
        };
        chType();
        chSup();
        clickOK();
    };

    gotoQuest();
    console.log('here');
    chooseSupporter(4, SUP_STRATEGY);

};


var battle = function() {
    var clickOK = function(popType){
        var _ = popType+
                ' > .prt-popup-footer'+
                ' > .btn-usual-ok';
        click(_);
        return true;
    };

    //第i个人施放第j个技能, 成功返回true
    var cast = function(i, j) {
        //选人
        var chr = $('.prt-member > .lis-character'+
                i.toString()+
                '.btn-command-character');
        if (chr.size()<=0) {
            return false;
        }
        click(chr);
        //选技能
        var ablty = nth('.prt-command-chara.chara'+
                (i+1).toString()+
                ' > .prt-ability-list > .lis-ability.btn-ability-available', j);
        if (ablty.size()<=0) {
            return false;
        } else {
            click(ablty);
        }
        //确认施法
        if (!clickOK('.prt-ability-dialog')) {
            return false;
        }
        //点返回
        click('.btn-command-back.display-on');
        return true;
    }
    //战斗结束点OK
    clickOK('.pop-exp');
}
